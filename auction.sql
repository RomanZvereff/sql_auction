CREATE DATABASE IF NOT EXISTS auction;

USE auction;

create table seller (
	id int(11) auto_increment primary key,
	seller_first_name varchar(10),
	seller_last_name varchar(10),
	seller_phone_number numeric(12)
);

create table product (
	id int(11) auto_increment primary key,
	product_name varchar(20),
	product_type varchar(30),
	product_address varchar(50),
	product_start_price numeric(10, 2),
	product_date_of_sale date,
	product_status varchar(2)
);

create table buyer (
	id int(11)auto_increment primary key,
	buyer_first_name varchar(10),
	buyer_last_name varchar(10),
	buyer_phone numeric(12),
	buyer_bid numeric(10, 2)
);

create table bid (
	id int(11)auto_increment primary key,
	bid_size int(10)
);

alter table product 
	add column fk_seller_id int (11);
alter table product 
	add constraint foreign key (fk_seller_id) references seller(id);
    
alter table bid
	add column fk_product_id int(11);
alter table bid
	add constraint foreign key (fk_product_id) references product(id);
	
alter table bid
	add column fk_buyer_id int(11);
alter table bid
	add constraint foreign key (fk_buyer_id) references buyer(id);